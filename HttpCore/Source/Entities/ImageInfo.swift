//
//  Image.swift
//  HttpCore
//
//  Created by Kukin Valentin on 8/10/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

public struct ImageInfo {
    public let thumbURL: String
    public let regularURL: String
    public let authorName: String
}
