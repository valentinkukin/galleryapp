//
//  VideoInfo.swift
//  HttpCore
//
//  Created by Kukin Valentin on 8/21/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation

public struct VideoInfo {
    public let caption: String
    public let videoURL: String
    public let imageURL: String
}
