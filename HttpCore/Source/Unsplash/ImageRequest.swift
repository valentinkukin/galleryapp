//
//  ImageDownloader.swift
//  HttpCore
//
//  Created by Kukin Valentin on 7/17/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import Alamofire

public protocol ImageRequestDelegate : class {
    func didFail(with error: ImageRequestErrors)
    func didLoad(images: [ImageInfo])
}

public class RequestBuilder {
    private let request: ImageRequest
    
    public init(_ factoryMethod: () -> ImageRequest) {
        self.request = factoryMethod()
    }
    
    public func route(_ route: Route) -> RequestBuilder {
        request.route = route
        return self
    }
    
    public func make() -> ImageRequest {
        return request
    }
}

public enum Route : String {
    case photos = "photos"
    case search = "search/photos"
}

public class ImageRequest {
    private let requestQueue = DispatchQueue(label: "imageRequestQueue")
    private let baseUrl = "https://api.unsplash.com/"
    private let apiKey = "bc277a6d26092281976c5f0210a8997a58de8e82a848c2b506a61fdf001302ca"
    private let parser: ImageParser
    
    var route: Route?
    public var params: Parameters
    var headers: HTTPHeaders = [:]
    
    public weak var delegate: ImageRequestDelegate?
    
    public init(parameters: Parameters, parser: @escaping ImageParser) {
        self.params = parameters
        self.parser = parser
        headers = [
            "Authorization": "Client-ID \(apiKey)",
            "Accept": "application/json"
        ]
    }
    
    public func setPage(_ page: Int) {
        params["page"] = page
    }
    
    public func send() {
        Alamofire
            .request(baseUrl + route!.rawValue, parameters: params, headers: headers)
            .validate()
            .responseJSON(queue: requestQueue) { response in
                switch response.result {
                case let .success(json):
                    let images = self.parser(json)
                    DispatchQueue.main.async {
                        self.delegate?.didLoad(images: images)
                    }
                case .failure:
                    DispatchQueue.main.async {
                        self.delegate?.didFail(with: ImageRequestErrors.serverError)
                    }
                }
        }
    }
}

public enum ImageRequestErrors : LocalizedError {
    case serverError
    
    public var errorDescription: String? {
        switch self {
        case .serverError: return "Server error"
        }
    }
}

