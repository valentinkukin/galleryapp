//
//  ImagesParsers.swift
//  HttpCore
//
//  Created by Kukin Valentin on 8/13/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation

public typealias ImageParser = (_ data: Any?) -> [ImageInfo]

public struct Parsers {
    
    public static var searchImagesParser: ImageParser {
        return { data in
            guard let info = data as? [String: Any], let imagesInfoList = info[.results] as? [[String: Any]] else { return [] }
            return parsedImages(imagesInfoList: imagesInfoList)
        }
    }
    
    public static var defaultImagesParser: ImageParser {
        return { data in
            guard let imagesInfoList = data as? [[String: Any]] else { return [] }
            return parsedImages(imagesInfoList: imagesInfoList)
        }
    }
    
    private static func parsedImages(imagesInfoList: [[String: Any]]) -> [ImageInfo] {
        var images: [ImageInfo] = []
        for imageInfo in imagesInfoList {
            let image = parsedImage(imageInfo: imageInfo)
            images.append(image)
        }
        return images
    }
    
    private static func parsedImage(imageInfo: [String: Any]) -> ImageInfo {
        var thumbURL = "" , regularURL = "", authorName = ""
        
        if let urls = imageInfo[.urls] as? [String: String] {
            thumbURL = urls[.thumb] ?? ""
            regularURL = urls[.regular] ?? ""
        }
        if let userInfo = imageInfo[.user] as? [String: Any] {
            if let firstName = userInfo[.firstName] as? String {
                authorName += firstName + " "
            }
            if let lastName = userInfo[.lastName] as? String {
                authorName += lastName
            }
        }
        return ImageInfo(thumbURL: thumbURL, regularURL: regularURL, authorName: authorName)
    }
}

fileprivate enum ImageParsingKeys: String {
    case results = "results"
    case urls = "urls"
    case thumb = "thumb"
    case regular = "regular"
    case user = "user"
    case firstName = "first_name"
    case lastName = "last_name"
}

extension Dictionary where Key == String, Value == Any {
    fileprivate subscript(key: ImageParsingKeys) -> Value? {
        return self[key.rawValue]
    }
}
extension Dictionary where Key == String, Value == String {
    fileprivate subscript(key: ImageParsingKeys) -> Value? {
        return self[key.rawValue]
    }
}
