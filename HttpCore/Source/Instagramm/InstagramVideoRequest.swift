//
//  InstagrammVideoRequest.swift
//  HttpCore
//
//  Created by Kukin Valentin on 8/21/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import Alamofire

public protocol InstagramVideoRequestDelegate: class {
    func didLoad(_ videoInfo: VideoInfo)
    func didFailInLoading()
}

public class InstagramVideoRequest {
    private let requestQueue = DispatchQueue(label: "videoRequestQueue")
    private let baseUrl = "https://api.instagram.com/v1/users/self/media/recent/"
    private let accessToken = "8468097535.1677ed0.7656293aeeba470faf51ee264c219680"
    
    public weak var delegate: InstagramVideoRequestDelegate?
    
    public init() {}
    
    public func send() {
        Alamofire
            .request(baseUrl, parameters: ["access_token": accessToken])
            .validate()
            .responseJSON(queue: requestQueue) { response in
                switch response.result {
                case let .success(json):
                    let videoInfo = self.parse(json)
                    DispatchQueue.main.async {
                        self.delegate?.didLoad(videoInfo)
                    }
                case .failure:
                    DispatchQueue.main.async {
                         self.delegate?.didFailInLoading()
                    }
                }
        }
    }
    
    private func parse(_ json: Any) -> VideoInfo {
        var url = "", caption = "Video", imageURL = ""
        
        guard let json = json as? [String: Any], let data = json[.data] as? [[String: Any]] else {
            return VideoInfo(caption: caption, videoURL: url, imageURL: imageURL)
        }
        
        if let captionInfo = data.first?[.caption] as? [String: Any]{
            caption = captionInfo[.text] as? String ?? caption
        }
        if let imageInfo = data.first?[.images] as? [String: Any] {
            if let standartResolutionInfo = imageInfo[.standardResolution] as? [String: Any] {
                imageURL = standartResolutionInfo[.url] as? String ?? ""
            }
        }
        
        guard let videosInfo = data.first?[.videos] as? [String: Any] else {
            return VideoInfo(caption: caption, videoURL: url, imageURL: imageURL)
        }
        if let standartResolutionInfo = videosInfo[.standardResolution] as? [String: Any] {
            url = standartResolutionInfo[.url] as? String ?? ""
        }
        return VideoInfo(caption: caption, videoURL: url, imageURL: imageURL)
    }
    
}

fileprivate enum VideoParsingKyes: String {
    case data = "data"
    case caption = "caption"
    case text = "text"
    case images = "images"
    case videos = "videos"
    case lowResolution = "low_resolution"
    case standardResolution = "standard_resolution"
    case url = "url"
}

extension Dictionary where Key == String, Value == Any {
    fileprivate subscript(key: VideoParsingKyes) -> Value? {
        return self[key.rawValue]
    }
}
extension Dictionary where Key == String, Value == String {
    fileprivate subscript(key: VideoParsingKyes) -> Value? {
        return self[key.rawValue]
    }
}
