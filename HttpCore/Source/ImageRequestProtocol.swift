//
//  ImageRequestProtocol.swift
//  HttpCore
//
//  Created by Kukin Valentin on 7/19/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import UIKit

public protocol ImageRequestProtocol {
    var url: String { get }
    var apiKey: String { get }
    var params: Parameters? { get }
    var headers: HTTPHeaders {get}
    var imagesPerPage: Int {get}
    weak var delegate: ImageRequestDelegate? { get set }
    
    init()
    
    func setPage(_ page: Int)
    func send()
    func pasre(json: Any) -> [Image]
}

extension ImageRequestProtocol {
    //private let requestQueue = DispatchQueue(label: "imageRequestQueue")
    
    public func send() {
        Alamofire
            .request(url, parameters: params, headers: headers)
            .validate()
            .responseJSON(queue: requestQueue) { response in
                switch response.result {
                case let .success(json):
                    let images = self.parse(json: json)
                    DispatchQueue.main.async {
                        self.delegate?.didLoad(images: images)
                    }
                case .failure:
                    DispatchQueue.main.async {
                        self.delegate?.didFail(with: ImageRequestErrors.serverError)
                    }
                }
        }
    }
}
