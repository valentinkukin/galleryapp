//
//  SearchRequest.swift
//  HttpCore
//
//  Created by Kukin Valentin on 7/19/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

public class SearchRequest : ImageRequest {
    override var url: String { return "https://api.unsplash.com/search/photos" }
    
    override public init() {
        super.init()
        params!["query"] = "manson"
    }
    
    override func parse(json: Any) -> [Image] {
        guard let info = json as? [String: Any], let imagesInfoList = info["results"] as? [[String: Any]] else {
            
            return []
        }
        print(imagesInfoList.count)
        var images: [Image] = []

        for imageInfo in imagesInfoList {
        var width: Int64 = 400, height: Int64 = 400
        var url = ""

        if let links = imageInfo["urls"] as? [String: String], let downloadLink = links["raw"] {
        url = downloadLink
        }
        if let imageWidth = imageInfo["width"] as? Int64, let imageHeight = imageInfo["height"] as? Int64 {
        width = imageWidth
        height = imageHeight
        }
        images.append(Image(url: url, size: Image.Size(width: width, height: height)))
        }
        return images
    }
}


