//
//  PhotosManager.swift
//  PhotosCore
//
//  Created by Kukin Valentin on 8/14/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import Photos
import UIKit

public class PhotosManager {
    public static let shared = PhotosManager()
    private let photosQueue = DispatchQueue(label: "photosQueue")
    public init() { }
    
    public func loadAssets(from album: String, comletion: ((PHFetchResult<PHAsset>) -> Void)? = nil) {
        photosQueue.async {
            guard let assetCollection = self.fetchAssetCollection(named: album) else {
                return
            }
            let photoAssets = PHAsset.fetchAssets(in: assetCollection, options: nil)
            DispatchQueue.main.async {
                comletion?(photoAssets)
            }
        }
    }
    
    public func save(image: UIImage, to album: String , completion:((Bool)->Void)? = nil) {
        photosQueue.async {
            if let assetCollection = self.fetchAssetCollection(named: album) {
                self.save(image, to: assetCollection, withCallback: completion)
                return
            }
            
            PHPhotoLibrary.shared().performChanges({
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: album)
            }) { success, error in
                guard success else {
                    return
                }
                if let assetCollection = self.fetchAssetCollection(named: album) {
                    self.save(image, to: assetCollection, withCallback: completion)
                }
            }
        }
    }
    
    private func save(_ image: UIImage, to assetCollection: PHAssetCollection, withCallback:((Bool)->Void)?) {
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection)
            let assetEnumeration:NSArray = [assetPlaceholder!]
            albumChangeRequest!.addAssets(assetEnumeration)
        }) { sucess, error in
            guard let callback = withCallback else {
                return
            }
            DispatchQueue.main.async {
                callback(sucess && error == nil)
            }
        }
    }
    
    private func fetchAssetCollection(named album: String) -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", album)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        return collection.firstObject
    }
}

fileprivate let imageSettingQueue = DispatchQueue(label: "imageSettingQueue")
extension UIImageView {

    public func setImage(with asset: PHAsset, size: CGSize, placeHolder: UIImage?) {
        self.image = placeHolder
        
        imageSettingQueue.async {
            let imageManager = PHCachingImageManager()
            let options = PHImageRequestOptions()
            options.deliveryMode = .fastFormat
            options.isSynchronous = true
            options.resizeMode = .exact
            
            imageManager.requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: options) { image, _ in
                guard let image = image else {
                    return
                }
                DispatchQueue.main.async {
                    self.image = image
                }
            }
        }
    }
}
