//
//  Extensions.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/10/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit
import AVKit

extension UICollectionViewCell {
    static var reuseID: String {
        return NSStringFromClass(self)
    }
}

extension UITableViewCell {
    static var reuseID: String {
        return NSStringFromClass(self)
    }
}
