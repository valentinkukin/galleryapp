//
//  SavedPhotosViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/2/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SavedPhotosViewController: UIViewController {
    
    private let feedModel = PhotosFeedModel()
    private var sellectedCellFrame: CGRect = .zero

    private weak var collectionView: UICollectionView?
    
    override func loadView() {
        self.view = SavedPhotosView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let savedPhotosView = view as? SavedPhotosView
        collectionView = savedPhotosView?.collectionView
        collectionView?.dataSource = self
        collectionView?.delegate = self
        feedModel.delegate = self
        
        feedModel.loadAssets { [weak self] in self?.collectionView?.reloadData() }
        
        initializeNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = .white
    }
}

extension SavedPhotosViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedModel.assetsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SavedPhotoCollectionViewCell.reuseID, for: indexPath)
            as! SavedPhotoCollectionViewCell
        
        feedModel.setImage(at: indexPath.row, for: cell.imageView, compressed: true, placeHolder: GalleryUI.placeHolder)
        return cell
    }
}

extension SavedPhotosViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow = GalleryUI.itemsPerRow
        let padding = GalleryUI.sectionInsets.left * CGFloat(itemsPerRow + 1)
        let width = (collectionView.bounds.width - padding) / CGFloat(itemsPerRow)
        let itemWidth = width
        let itemHeight = width
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return GalleryUI.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return GalleryUI.sectionInsets.left
    }
}

extension SavedPhotosViewController: PhotosFeedModelDelegate {
    func didSavePhotos(at indexPath: [IndexPath]) {
        collectionView?.performBatchUpdates({
            collectionView?.insertItems(at: indexPath)
        }, completion: nil)
    }
    
    func didRemovePhotos(at indexPath: [IndexPath]) {
        collectionView?.performBatchUpdates({
            collectionView?.deleteItems(at: indexPath)
        }, completion: nil)
    }
    
    func didChangePhotos(at indexPath: [IndexPath]) {
        collectionView?.performBatchUpdates({
            collectionView?.reloadItems(at: indexPath)
        }, completion: nil)
    }

}

extension SavedPhotosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = (collectionView.cellForItem(at: indexPath)) as? SavedPhotoCollectionViewCell
        if let cell = cell {
            sellectedCellFrame = cell.convert(cell.imageView.frame, to: nil)
        }
        let index = indexPath.row 
        let placeHolder = cell?.imageView.image
        let vc = ImageDetailsViewController(imageIndex: index, feedModel: feedModel, placeHolder: placeHolder)
        vc.isEnableSaving = false
        
        self.navigationController?.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SavedPhotosViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard toVC is ImageDetailsViewController || fromVC is ImageDetailsViewController else {
            return nil
        }
        
        let isPresent = operation == .push
        return ImageDetailsTransition(duration: 0.2, isPresenting: isPresent, originFrame: sellectedCellFrame)
    }
}

extension SavedPhotosViewController {
    private func initializeNavigationBar() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.title = PhotosSettings.album
    }
}


