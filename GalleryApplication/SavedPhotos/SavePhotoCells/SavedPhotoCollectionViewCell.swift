//
//  SavedPhotoCollectionViewCell.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/2/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SavedPhotoCollectionViewCell: UICollectionViewCell {
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = contentView.bounds
    }
}

extension SavedPhotoCollectionViewCell {
    private func initializeViews() {
        initializeContentView()
        initializeImageView()
    }
    
    private func initializeContentView() {
        contentView.backgroundColor = Color.cellBackground
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = Color.cellBorder.cgColor
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
    }
    
    private func initializeImageView() {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
    }
}
