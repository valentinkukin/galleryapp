//
//  SavedPhotosView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/9/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SavedPhotosView: UIView {
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionView.register(SavedPhotoCollectionViewCell.self, forCellWithReuseIdentifier: SavedPhotoCollectionViewCell.reuseID)
        collectionView.backgroundColor = .white
        addSubview(collectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        collectionView.frame = bounds
    }
}
