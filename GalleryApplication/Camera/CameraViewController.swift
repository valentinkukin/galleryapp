//
//  CameraViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/28/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import AVKit
import PhotosCore

class CameraViewController: UIViewController {
    
    private let captureSession = AVCaptureSession()
    private var capturePhotoOutput: AVCapturePhotoOutput?
    private weak var backButton: UIButton?
    private weak var takePhotoButton: UIButton?
    
    override func loadView() {
        view = CameraView(captureSession: captureSession)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cameraView = view as? CameraView
        backButton = cameraView?.backButton
        takePhotoButton = cameraView?.takePhotoButton
        
        backButton?.addTarget(self, action: #selector(backButtonClick), for: .touchUpInside)
        takePhotoButton?.addTarget(self, action: #selector(takePhotoButtonClick), for: .touchUpInside)

        initializeCaptureSession()
    }
    
    private func initializeCaptureSession() {
        let captureDevice = AVCaptureDevice.default(for: .video)
        if let captureDevice = captureDevice, let input = try? AVCaptureDeviceInput(device: captureDevice) {
            captureSession.addInput(input)
        }
        
        capturePhotoOutput = AVCapturePhotoOutput()
        capturePhotoOutput?.isHighResolutionCaptureEnabled = true
        captureSession.addOutput(capturePhotoOutput!)
        
        DispatchQueue(label: "startSessionQueue").async { [weak self] in
            self?.captureSession.startRunning()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    @objc private func backButtonClick(_ sender: UIButton) {
        tabBarController?.selectedIndex = 0
        tabBarController?.tabBar.isHidden = false
    }
    
    @objc private func takePhotoButtonClick(_ sender: UIButton) {
        guard let capturePhotoOutput = capturePhotoOutput else {
            return
        }
        let photoSettings = AVCapturePhotoSettings()
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = .auto

        capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
        showTakePhotoAnimation()
    }
    
    private func showTakePhotoAnimation() {
        let jumperView = UIView(frame: view.frame)
        jumperView.backgroundColor = .black
        view.addSubview(jumperView)
        UIView.animate(withDuration: 0.3, animations: {
            jumperView.alpha = 0
        }, completion: { (_) in
            jumperView.removeFromSuperview()
        })
    }
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {

        guard let imageData = photo.fileDataRepresentation() else {
            return
        }
        guard let uiImage = UIImage(data: imageData) else {
            return
        }
        guard let cgImage = uiImage.cgImage else {
            return
        }
        
        let deviceOrientation = UIDevice.current.orientation
        let image = UIImage(cgImage: cgImage, scale: 1.0, orientation: deviceOrientation.getUIImageOrientation())
        
        PhotosManager.shared.save(image: image, to: PhotosSettings.album)
    }
}

extension UIDeviceOrientation {
    func getUIImageOrientation() -> UIImage.Orientation {
        switch self {
        case .portrait, .faceUp: return .right
        case .portraitUpsideDown, .faceDown: return .left
        case .landscapeLeft: return .up
        case .landscapeRight: return .down
        case .unknown: return .right
        }
    }
}
