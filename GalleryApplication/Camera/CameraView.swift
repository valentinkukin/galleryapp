//
//  CameraView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/28/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import AVKit

class CameraView: UIView {
    let videoPreviewLayer: AVCaptureVideoPreviewLayer
    
    let backButton = UIButton()
    let takePhotoButton = UIButton()
    
    init(captureSession: AVCaptureSession) {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        super.init(frame: .zero)
        videoPreviewLayer.videoGravity = .resizeAspectFill
        layer.addSublayer(videoPreviewLayer)
        initializeButtons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        videoPreviewLayer.frame = layer.bounds
        backButton.frame = CGRect(x: 10, y: 20, width: 35, height: 35)
        takePhotoButton.frame = CGRect(x: bounds.midX - 25, y: bounds.maxY - 100, width: 50, height: 50)
    }
    
    private func initializeButtons() {
        backButton.setImage(UIImage(named: "backarrow.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = UIColor.white.withAlphaComponent(0.8)
        addSubview(backButton)
        
        takePhotoButton.setImage(UIImage(named: "circle.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        takePhotoButton.tintColor = UIColor.white.withAlphaComponent(0.8)
        addSubview(takePhotoButton)
    }
}
