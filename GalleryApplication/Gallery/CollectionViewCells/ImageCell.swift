//
//  ImageCell.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/17/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {

    let imageView = UIImageView()
    let authorNameLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let parent = contentView.bounds
        imageView.frame = CGRect(x: 0, y: 0, width: parent.width, height: parent.height - parent.width/3)
        authorNameLabel.frame = CGRect(x: parent.width * 0.05, y: imageView.frame.maxY,
                                       width: parent.width * 0.9, height: parent.maxY - imageView.frame.maxY)
    }
}

extension ImageCell {
    private func initializeViews() {
        initializeContentView()
        initializeImageView()
        initializeAuthorNameLabel()
    }
    
    private func initializeContentView() {
        contentView.backgroundColor = Color.cellBackground
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = Color.cellBorder.cgColor
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
    }
    
    private func initializeImageView() {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
    }
    
    private func initializeAuthorNameLabel() {
        authorNameLabel.numberOfLines = 2
        authorNameLabel.adjustsFontSizeToFitWidth = true
        authorNameLabel.text = "Author Name"
        authorNameLabel.textAlignment = .center
        authorNameLabel.textColor = Color.authorName
        contentView.addSubview(authorNameLabel)
    }
}


