//
//  GalleryViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    private var presentTransition: UIViewControllerAnimatedTransitioning?
    private var dissmisTransition: UIViewControllerAnimatedTransitioning?
    
    private let feedModel = GalleryFeedModel()
    private var sellectedCellFrame: CGRect = .zero
    private var isLoadingImages = false
    
    private weak var collectionView: UICollectionView?
    private weak var refreshControl: UIRefreshControl? { return collectionView?.refreshControl }
    private weak var searchBar: UISearchBar? { return navigationItem.titleView as? UISearchBar }
    
    override func loadView() {
        self.view = GalleryView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let galleryView = view as? GalleryView
        collectionView = galleryView?.collectionView
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        feedModel.delegate = self
        feedModel.loadImages()
        
        initializeNavigationBar()
        initializeRefreshContorl()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = ThemeColor.current
        refreshControl?.tintColor = ThemeColor.current
    }
    
    private func initializeNavigationBar() {
        let image = UIImage(named: "menu.png")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(activitiesButtonClick))
        self.navigationItem.leftBarButtonItem?.tintColor = .white

        let searchBar = UISearchBar()
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
    }
    
    private func initializeRefreshContorl() {
        collectionView?.refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Loading images")
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
    }

    @objc private func refreshData(_ sender: Any) {
        feedModel.clear()
        collectionView?.reloadData()
        feedModel.loadImages()
    }
    
    @objc private func activitiesButtonClick(_ sender: UIButton) {
        searchBar?.endEditing(true)
        
        let activitiesVC = ActivitiesViewController()
        activitiesVC.transitioningDelegate = self
        activitiesVC.modalPresentationStyle = .overFullScreen
        
        presentTransition = ActivitiesTransition(duration: 0.2, isPresent: true)
        dissmisTransition = ActivitiesTransition(duration: 0.2, isPresent: false)
        present(activitiesVC, animated: true, completion: nil)
    }
    
    private func displayDefaultImages() {
        clear()
        feedModel.loadImages()
    }
    private func displayFoundImages(for query: String) {
        clear()
        feedModel.loadImages(query: query)
    }
    private func clear() {
        feedModel.clear()
        collectionView?.reloadData()
        showActivityIndicator()
    }
    
    private func showActivityIndicator() {
        collectionView?.backgroundView = activityIndicator
        activityIndicator.startAnimating()
    }
}

extension GalleryViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedModel.imagesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.reuseID, for: indexPath) as! ImageCell
        
        feedModel.setImage(at: indexPath.row, for: cell.imageView, compressed: true, placeHolder: GalleryUI.placeHolder)
        cell.authorNameLabel.text = feedModel.images[indexPath.row].authorName
        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let dHeight = scrollView.contentSize.height - scrollView.bounds.height
        if !isLoadingImages && scrollView.contentOffset.y >= dHeight, !(refreshControl?.isRefreshing ?? false)  {
            self.isLoadingImages = true
            feedModel.loadImagesFromNextPage()
        }
    }
}

extension GalleryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = (collectionView.cellForItem(at: indexPath)) as? ImageCell
        if let cell = cell {
            sellectedCellFrame = cell.convert(cell.imageView.frame, to: nil)
        }
        let index = indexPath.row
        let placeHolder = cell?.imageView.image
        let title = "by: \(feedModel.images[index].authorName)"
        let vc = ImageDetailsViewController(imageIndex: index, feedModel: feedModel, placeHolder: placeHolder, title: title)

        self.navigationController?.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow = GalleryUI.itemsPerRow
        let padding = GalleryUI.sectionInsets.left * CGFloat(itemsPerRow + 1)
        let width = (collectionView.bounds.width - padding) / CGFloat(itemsPerRow)
        let itemWidth = width
        let itemHeight = width + width/3
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return GalleryUI.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return GalleryUI.sectionInsets.left
    }
}

extension GalleryViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text else {
            return
        }
        searchBar.resignFirstResponder()
        displayFoundImages(for: query)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if feedModel.state == .inSearch && searchBar.text == "" {
            displayDefaultImages()
        } 
    }
}

extension GalleryViewController: FeedModelDelegate {
    func didLoadImages(count: Int) {
        collectionView?.backgroundView = nil
        insertImages(count: count)
    }
    
    private func insertImages(count: Int) {
        let beginItem = collectionView?.numberOfItems(inSection: 0) ?? 0
        let indexPaths = (0..<count).map { IndexPath(item: beginItem + $0, section: 0) }
        collectionView?.performBatchUpdates({
            collectionView?.insertItems(at: indexPaths)
        }, completion: { _ in
            self.isLoadingImages = false
            self.hideActivityIndicator()
            self.refreshControl?.endRefreshing()
        })
    }
    
    private func hideActivityIndicator() {
        self.collectionView?.backgroundView = nil
        self.activityIndicator.stopAnimating()
    }
    
    func didFailInLoadingImages(errorText: String?) {
        setErrorView(errorText: errorText)
    }
    
    func setErrorView(errorText: String?) {
        guard collectionView?.numberOfItems(inSection: 0) == 0 else {
            return
        }
        let errorLabel = UILabel()
        errorLabel.text = errorText ?? "Error of loading data"
        errorLabel.textAlignment = .center
        errorLabel.backgroundColor = Color.errorBackground
        collectionView?.backgroundView = errorLabel
    }
}

extension GalleryViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        searchBar?.endEditing(true)
    }
}

extension GalleryViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard toVC is ImageDetailsViewController || fromVC is ImageDetailsViewController else {
            return nil
        }
        let isPresent = (operation == .push) ? true : false
        return ImageDetailsTransition(duration: 0.2, isPresenting: isPresent, originFrame: sellectedCellFrame)
    }
}

extension GalleryViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presentTransition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dissmisTransition
    }
}
