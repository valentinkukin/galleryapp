//
//  ImageDetailsView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/8/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ImageDetailsView: UIView {
    
    let scrollView = UIScrollView()
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        scrollView.backgroundColor = .white
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.clipsToBounds = false
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 5
        addSubview(scrollView)
        scrollView.addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        scrollView.frame = bounds
        
        // Next code centers image view using image size, 'cause image view is zomming
        // If image view frame is self.bounds, the empty zones (whitch will be not covered by image) will zomming too
        let imageSize = imageView.image?.size ?? bounds.size
        let width = bounds.width
        let height = imageSize.height * (width / imageSize.width)
        let y = (bounds.height - height) * 0.5
        imageView.frame = CGRect(x: 0, y: y, width: width, height: height)
    }
}
