//
//  ImageDetailsViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/23/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import PhotosCore

class ImageDetailsViewController: UIViewController {
    
    var isEnableSaving = true
    private let imageIndex: Int
    private let imageFeedModel: ImageFeedModel
    private let placeHolder: UIImage?
    private let imageTitle: String
    private var isSaveButtonEnable = true
    private var statusBarHidden = false
    private weak var scrollView: UIScrollView?
    private weak var imageView: UIImageView?
    
    init(imageIndex: Int, feedModel: ImageFeedModel, placeHolder: UIImage? = UIImage(named: "grey.jpg"), title: String = "" ) {
        self.imageIndex = imageIndex
        self.imageFeedModel = feedModel
        self.placeHolder = placeHolder
        self.imageTitle = title
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = ImageDetailsView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        let imageDetailsView = self.view as? ImageDetailsView
        scrollView = imageDetailsView?.scrollView
        imageView = imageDetailsView?.imageView
        
        scrollView?.delegate = self
        
        initializeTapRecognaizers()
        initializeViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = ThemeColor.current
        navigationController?.navigationBar.barTintColor = .white
        self.navigationItem.rightBarButtonItem?.tintColor = ThemeColor.current
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        centrateImage()
    }
    
    override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }

    @objc private func saveButtonClick(_ sender: UIButton) {
        guard let image = imageView?.image, isSaveButtonEnable == true else {
            return
        }
        
        isSaveButtonEnable = false
        guard image == placeHolder else {
            save(image)
            return
        }
        let ac = UIAlertController(title: "The image hasn't loaded in hight quality yet!", message: "Do you want to save it?", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Yes", style: .default) { _ in self.save(image) })
        ac.addAction(UIAlertAction(title: "No", style: .default) { _ in self.isSaveButtonEnable = true })
        present(ac, animated: true)
    }
    
    private func save(_ image: UIImage) {
        PhotosManager.shared.save(image: image, to: PhotosSettings.album) { success in
            guard success else {
                let ac = UIAlertController(title: "Save error", message: "The image has not saved to album", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(ac, animated: true)
                return
            }
            let ac = UIAlertController(title: "Saved!", message: "The image has been saved to album.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)

            self.isSaveButtonEnable = true
        }
    }

    @objc private func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        guard let scrollView = scrollView, let _ = imageView else {
            return
        }
        let avarageZoomScale = getAvarageZoomScale()
        if scrollView.zoomScale != scrollView.minimumZoomScale {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.zoom(to: zoomRectForScale(scale: avarageZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        }
    }
    
    private func getAvarageZoomScale () -> CGFloat {
        guard let scrollView = scrollView else {
            return 1
        }
        return (scrollView.minimumZoomScale + scrollView.maximumZoomScale) / 3
    }
    
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        guard let scrollView = scrollView, let imageView = imageView else {
            return .zero
        }
        
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    @objc private func singleTap(_ sender: UITapGestureRecognizer) {
        barsToggle(isVisible: statusBarHidden)
        centrateImage()
    }
    
    private func barsToggle(isVisible: Bool) {
        statusBarHidden = !isVisible
        setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.setNavigationBarHidden(!isVisible, animated: true)
        UIView.animate(withDuration: 0.5) {
            self.scrollView?.backgroundColor = isVisible ? .white : .black
            self.tabBarController?.tabBar.isHidden = !isVisible
        }
    }
}

extension ImageDetailsViewController: UIScrollViewDelegate {

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centrateImage()
    }
    
    private func centrateImage() {
        guard let scrollView = scrollView, let imageView = imageView else {
            return
        }
        let offsetX = max(CGFloat(scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0)
        let offsetY = max(CGFloat(scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0)
        let center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX, y: scrollView.contentSize.height * 0.5 + offsetY)
        imageView.center = center
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        barsToggle(isVisible: false)
    }
}

extension ImageDetailsViewController: ImageDetailsTransitionedController {
    var fadeView: UIView? {
        return scrollView
    }

    var compressedView: UIView? {
        return imageView
    }
}

extension ImageDetailsViewController {
    private func initializeViews() {
        initializeNavigationBar()
        initializeImageView()
    }
    
    private func initializeTapRecognaizers() {
        let singleTapGest = UITapGestureRecognizer(target: self, action: #selector(singleTap))
        singleTapGest.numberOfTapsRequired = 1
        scrollView?.addGestureRecognizer(singleTapGest)
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView))
        doubleTapGest.numberOfTapsRequired = 2
        scrollView?.addGestureRecognizer(doubleTapGest)
        
        singleTapGest.require(toFail: doubleTapGest)
    }
    
    private func initializeNavigationBar() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.title = imageTitle

        if isEnableSaving {
            let image = UIImage(named: "save.png")
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(saveButtonClick))
        }
    }
    
    private func initializeImageView() {
        guard let imageView = imageView else {
            return
        }
        imageFeedModel.setImage(at: imageIndex, for: imageView, compressed: false, placeHolder: placeHolder)
        view.setNeedsLayout()
    }
}
