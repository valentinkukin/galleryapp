//
//  InstagramVideoView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/21/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import AVKit

class InstagramVideoView: UIView {
    let captionLabel = UILabel()
    let imageView = UIImageView()
    let playButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        initializeCaptionLabel()
        addSubview(imageView)
        initializePlayButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        let captionLabelHeight = bounds.height/8
        captionLabel.frame = CGRect(x: 0, y: 0, width: bounds.width, height: captionLabelHeight)
        
        imageView.frame = bounds
        imageView.contentMode = .scaleAspectFit
        
        if let image = imageView.image {
            playButton.frame = AVMakeRect(aspectRatio: image.size, insideRect: imageView.bounds)
        } else {
            playButton.frame.size = CGSize(width: 100, height: 100)
            playButton.center = center
        }
    }
    
    private func initializeCaptionLabel() {
        captionLabel.font = .systemFont(ofSize: 25)
        captionLabel.textColor = .black
        captionLabel.textAlignment = .center
        addSubview(captionLabel)
    }
    
    private func initializePlayButton() {
        playButton.setImage(UIImage(named: "playVideo.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playButton.tintColor = UIColor.black.withAlphaComponent(0.8)
        addSubview(playButton)
    }
}
