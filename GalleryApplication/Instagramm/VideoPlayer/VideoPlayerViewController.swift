//
//  VideoPlayerViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/23/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import AVKit

protocol VideoPlayerDelegate: class {
    func willDismiss()
}

class VideoPlayerViewController: UIViewController {

    private let player: AVPlayer
    private var videoIsPlaying = false
    private weak var toggleButton: UIButton?
    private weak var cancelButton: UIButton?
    private weak var playbackSlider: UISlider?
    private weak var timeLabel: UILabel?
    
    weak var delegate: VideoPlayerDelegate?
    
    init(videoURL: URL) {
        player = AVPlayer(url: videoURL)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = VideoPlayerView(player: player)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let playerView = view as? VideoPlayerView
        toggleButton = playerView?.toggleButton
        cancelButton = playerView?.cencleButton
        playbackSlider = playerView?.playbackSlider
        timeLabel = playerView?.timeLabel
        
        toggleButton?.addTarget(self, action: #selector(toggleButtonClick), for: .touchUpInside)
        cancelButton?.addTarget(self, action: #selector(cancelButtonClick), for: .touchUpInside)
        playbackSlider?.addTarget(self, action: #selector(playbackSliderValueChanged), for: .valueChanged)
        playbackSlider?.addTarget(self, action: #selector(playbackSliderIsMoving), for: .touchDown)
        
        initializePlayerTimer()
        
        playVideo()
        initializeTapRecognaizers()
    }
    
    private func initializePlayerTimer() {
        player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { [weak self] _ in
            guard let weakSelf = self else {
                return
            }
            
            if weakSelf.player.currentItem?.status == .readyToPlay {
                let time = CMTimeGetSeconds(weakSelf.player.currentTime())
                weakSelf.playbackSlider?.value = Float(time)
                
                let totalTime = weakSelf.player.currentItem?.asset.duration.toString() ?? "00:00"
                let currentTime = weakSelf.player.currentTime().toString()
                weakSelf.timeLabel?.text = "\(currentTime)/\(totalTime)"
            }
        }
    }
    
    private func initializeTapRecognaizers() {
        let singleTapGest = UITapGestureRecognizer(target: self, action: #selector(singleTap))
        singleTapGest.numberOfTapsRequired = 1
        view.addGestureRecognizer(singleTapGest)
    }
    
    @objc private func singleTap(_ sender: UITapGestureRecognizer) {
        switchButton()
    }
    
    @objc private func toggleButtonClick(_ sender: UIButton) {
        switchButton()
    }
    
    func switchButton() {
        if player.timeControlStatus == .playing {
            pauseVideo()
        } else {
            playVideo()
        }
    }
    
    func pauseVideo() {
        player.pause()
        let playImage = UIImage(named: "play.png")?.withRenderingMode(.alwaysTemplate)
        toggleButton?.setImage(playImage, for: .normal)
    }
    
    func playVideo() {
        player.play()
        let pauseImage = UIImage(named: "pause.png")?.withRenderingMode(.alwaysTemplate)
        toggleButton?.setImage(pauseImage, for: .normal)
    }
    
    @objc private func cancelButtonClick(_ sender: UIButton) {
        delegate?.willDismiss()
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
    @objc private func playbackSliderIsMoving(_ playbackSlider:UISlider ) {
        player.pause()
    }
    
    @objc private func playbackSliderValueChanged(_ playbackSlider:UISlider)
    {
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        
        player.seek(to: targetTime)
        
        if player.timeControlStatus == .paused {
            playVideo()
        }
    }
}

extension CMTime {
    fileprivate func toString() -> String {
        let totalSeconds = Int(CMTimeGetSeconds(self).rounded())
        let hours = Int(totalSeconds / 3600)
        let minutes = Int(totalSeconds % 3600 / 60)
        let seconds = Int(totalSeconds % 60)
        
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}
