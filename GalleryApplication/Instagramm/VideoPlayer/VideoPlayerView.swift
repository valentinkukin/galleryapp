//
//  VideoPlayerView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/23/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import AVKit

class VideoPlayerView: UIView {

    private let playerLayer: AVPlayerLayer
    private var player: AVPlayer? { return playerLayer.player }
    let toggleButton = UIButton()
    let cencleButton = UIButton()
    let playbackSlider = UISlider()
    let timeLabel = UILabel()
    
    init(player: AVPlayer) {
        playerLayer = AVPlayerLayer(player: player)
        super.init(frame: .zero)
        backgroundColor = .black
        layer.addSublayer(playerLayer)
        initializeToggleButton()
        initializeCancleButton()
        initializeTimeLabel()
        initializePlaybackSlider()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        playerLayer.frame = bounds
        
        let sidePadding: CGFloat = 15
        let buttonSize = CGSize(width: 20, height: 20)
        toggleButton.frame = CGRect(x: sidePadding, y: bounds.maxY - buttonSize.height - sidePadding,
                                    width: buttonSize.width, height: buttonSize.height)
        cencleButton.frame = CGRect(x: sidePadding, y: sidePadding, width: buttonSize.width, height: buttonSize.height)
        
        let timeLabelWidth: CGFloat = 75
        timeLabel.frame = CGRect(x: bounds.maxX - timeLabelWidth - sidePadding, y: toggleButton.frame.minY,
                                 width: timeLabelWidth, height: buttonSize.height)
        
        playbackSlider.frame = CGRect(x: toggleButton.frame.maxX + sidePadding, y: toggleButton.frame.minY,
                                      width: bounds.width - toggleButton.frame.maxX - sidePadding * 3 - timeLabelWidth,
                                      height: buttonSize.height)
    }
    
    private func initializeToggleButton() {
        toggleButton.tintColor = Color.playerControlls
        addSubview(toggleButton)
    }
    
    private func initializeCancleButton() {
        cencleButton.setImage(UIImage(named: "cancel.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cencleButton.tintColor = Color.playerControlls
        addSubview(cencleButton)
    }
    
    private func initializeTimeLabel() {
        timeLabel.text = "00:00/00:00"
        timeLabel.textAlignment = .center
        timeLabel.textColor = Color.playerControlls
        timeLabel.font = .systemFont(ofSize: 12)
        addSubview(timeLabel)
    }
    
    private func initializePlaybackSlider() {
        playbackSlider.minimumValue = 0

        let duration : CMTime = playerLayer.player?.currentItem?.asset.duration ?? CMTime()
        let seconds : Float64 = CMTimeGetSeconds(duration)
        
        playbackSlider.thumbTintColor = Color.playerControlls
        playbackSlider.maximumTrackTintColor = Color.playerControlls
        playbackSlider.maximumValue = Float(seconds)
        playbackSlider.isContinuous = false
        playbackSlider.tintColor = UIColor.white
        addSubview(playbackSlider)
    }
}
