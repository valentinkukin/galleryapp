//
//  InstagramVideoErrorView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/22/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class InstagramVideoErrorView: UIView {
    
    let errorLabel = UILabel()
    let tryAgainButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Color.errorBackground
        initializeErrorLabel()
        initializeTryAgainButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        errorLabel.sizeToFit()
        errorLabel.center = CGPoint(x: bounds.width * 0.5, y: bounds.height * 0.5)
        
        tryAgainButton.frame.size = CGSize(width: errorLabel.frame.width, height: 50)
        let topPadding: CGFloat = 20
        tryAgainButton.center = CGPoint(x: bounds.width * 0.5, y: bounds.height * 0.5 + errorLabel.frame.height + topPadding)
    }
    
    private func initializeErrorLabel() {
        errorLabel.text = "Can not load video"
        errorLabel.textAlignment = .center
        errorLabel.font = .systemFont(ofSize: 20)
        addSubview(errorLabel)
    }
    
    private func initializeTryAgainButton() {
        tryAgainButton.setTitle("Try again", for: .normal)
        tryAgainButton.backgroundColor = UIColor.white .withAlphaComponent(0.6)
        tryAgainButton.setTitleColor(UIColor.gray.withAlphaComponent(0.7), for: .normal)
        tryAgainButton.layer.cornerRadius = 10
        tryAgainButton.layer.borderColor = UIColor.gray.cgColor
        tryAgainButton.layer.borderWidth = 1
        addSubview(tryAgainButton)
    }
    
}
