//
//  InstagramVideoViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/21/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit
import HttpCore
import AVKit

class InstagramVideoViewController: UIViewController {
    
    private let instagramRequest = InstagramVideoRequest()
    private var videoInfo: VideoInfo?
    private var player: AVPlayer?
    
    private weak var videoPlayerView: VideoPlayerView?
    private weak var activityIndicator: UIActivityIndicatorView?
    private weak var errorView: InstagramVideoErrorView?
    
    private weak var captionLabel: UILabel?
    private weak var previewImageView: UIImageView?
    private weak var playButton: UIButton?
    
    private weak var videoController: VideoPlayerViewController?
    
    override func loadView() {
        view = InstagramVideoView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let instagramView = view as? InstagramVideoView
        captionLabel = instagramView?.captionLabel
        previewImageView = instagramView?.imageView
        playButton = instagramView?.playButton
        
        playButton?.isHidden = true
        playButton?.addTarget(self, action: #selector(playVideoClick), for: .touchUpInside)
        
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])

        instagramRequest.delegate = self
        loadVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        videoController?.pauseVideo()
    }

    override func viewDidLayoutSubviews() {
        layoutVideo()
    }
    
    private func layoutVideo() {
        guard let imageView = previewImageView, let image = imageView.image else {
            videoController?.view.frame = CGRect(x: view.center.x - 150, y: view.center.y - 150, width: 300, height: 300)
            return
        }
        videoController?.view.frame = AVMakeRect(aspectRatio: image.size, insideRect: imageView.bounds)
    }
    
    private func loadVideo() {
        showActivityIndicator()
        instagramRequest.send()
    }

    private func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: view.bounds)
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.autoresizingMask = [.flexibleBottomMargin, .flexibleTopMargin,
                                              .flexibleLeftMargin, .flexibleRightMargin]
        self.activityIndicator = activityIndicator
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    @objc private func playVideoClick(_ sender: UIButton) {
        guard let string = videoInfo?.videoURL, let url = URL(string: string) else {
            return
        }
        playVideo(from: url)
    }
    
    private func playVideo(from url: URL) {
        previewImageView?.isHidden = true
        playButton?.isHidden = true
        
        let vc = VideoPlayerViewController(videoURL: url)
        vc.delegate = self
        videoController = vc
        addChild(vc)
        view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
}

extension InstagramVideoViewController: InstagramVideoRequestDelegate {
    func didLoad(_ videoInfo: VideoInfo) {
        self.videoInfo = videoInfo
        hideActivityIndicator()
        
        captionLabel?.text = videoInfo.caption
        if let url = URL(string: videoInfo.imageURL) {
            previewImageView?.af_setImage(withURL: url, placeholderImage: UIImage(named: "grey.png"))
        }
        self.view.setNeedsLayout()
        playButton?.isHidden = false
    }
    
    func didFailInLoading() {
        hideActivityIndicator()
        setErrorView()
    }
    
    private func hideActivityIndicator() {
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
    }
    
    private func setErrorView() {
        let errorView = InstagramVideoErrorView(frame: view.bounds)
        self.errorView = errorView
        errorView.autoresizingMask = [.flexibleBottomMargin, .flexibleTopMargin, .flexibleLeftMargin,
                                      .flexibleRightMargin, .flexibleWidth, .flexibleHeight]
        errorView.tryAgainButton.addTarget(self, action: #selector(reloadVideo), for: .touchUpInside)
        self.view.addSubview(errorView)
    }
    
    @objc private func reloadVideo(_ sender: UIButton) {
        errorView?.removeFromSuperview()
        loadVideo()
    }
}


extension InstagramVideoViewController: VideoPlayerDelegate {
    func willDismiss() {
        previewImageView?.isHidden = false
        playButton?.isHidden = false
    }
}
