//
//  UserSettings.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/7/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    static let cellBackground = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    static let cellBorder = UIColor(red:0.83, green:0.83, blue:0.83, alpha:1.0)
    static let authorName = UIColor(red:0.30, green:0.30, blue:0.30, alpha:1.0)
    static let errorBackground = UIColor(red: 0.1, green: 0.01, blue: 0.1, alpha: 0.05)
    static let playerControlls = UIColor.white.withAlphaComponent(0.7)
}

fileprivate let themeColorSaveKey = "themeColor"
struct ThemeColor {
    static var current = ThemeColor.color(for: UserDefaults.standard.string(forKey: themeColorSaveKey))
    static let red = UIColor(red:0.63, green:0.00, blue:0.24, alpha:1.0)
    static let blue = UIColor(red:0.00, green:0.33, blue:0.63, alpha:1.0)
    static let yellow = UIColor(red:0.63, green:0.55, blue:0.00, alpha:1.0)
    static let green = UIColor(red:0.00, green:0.48, blue:0.48, alpha:1.0)
    
    static func setCurrent(color: UIColor) {
        let key = color.toString()
        current = self.color(for: key)
        UserDefaults.standard.set(key, forKey: themeColorSaveKey)
    }
    
    private static func color(for key: String?) -> UIColor {
        switch key {
        case red.toString(): return red
        case green.toString(): return green
        case blue.toString(): return blue
        case yellow.toString(): return yellow
        default:
            NSLog("You should add color to ThemeColor struct!")
            return green
        }
    }
}

extension UIColor {
    fileprivate func toString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return "\(r)\(g)\(b)\(a)"
    }
}
