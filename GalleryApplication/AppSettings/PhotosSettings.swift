//
//  PhotosSettings.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/20/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation

struct PhotosSettings {
    static let album = "Saved Photos"
}
