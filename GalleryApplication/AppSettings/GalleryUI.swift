//
//  GalleryUI.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/20/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit

struct GalleryUI {
    static let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    static let itemsPerRow = 2
    static let placeHolder = UIImage(named: "noimage.jpg")
}
