//
//  ActivitiesTransition.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/30/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit

class ActivitiesTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration: TimeInterval
    let isPresent: Bool

    private weak var transitionContext: UIViewControllerContextTransitioning?
    private var containerView: UIView? { return transitionContext?.containerView }
    private var fromView: UIView? { return transitionContext?.view(forKey: UITransitionContextViewKey.from) }
    private var fromVC: UIViewController? { return transitionContext?.viewController(forKey: .from) }
    private var toView: UIView? { return transitionContext?.view(forKey: UITransitionContextViewKey.to) }
    private var toVC: UIViewController? { return transitionContext?.viewController(forKey: .to) }
    
    init(duration: TimeInterval, isPresent: Bool) {
        self.isPresent = isPresent
        self.duration = duration
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        if isPresent {
            present()
        } else {
            dissmiss()
        }
    }
    
    private func present() {
        guard let transitionContext = transitionContext, let container = containerView, let toView = toView, let fromView = fromVC?.view else {
            fatalError("Present transition Error")
        }
        container.addSubview(toView)
        
        toView.frame = fromView.bounds
        toView.frame.origin = CGPoint(x: -toView.frame.maxX, y: 0)
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            toView.frame.origin = CGPoint.zero
        } , completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
    
    private func dissmiss() {
        guard let transitionContext = transitionContext, let container = containerView, let fromView = fromView else {
            fatalError("Dismiss transition Error")
        }
        container.addSubview(fromView)
        
        fromView.frame.origin = .zero
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            fromView.frame.origin = CGPoint(x: -fromView.frame.maxX, y: 0)
        } , completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
    
}
