//
//  PhotoPresentAnimation.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/26/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit

protocol ImageDetailsTransitionedController : class {
    var compressedView: UIView? { get }
    var fadeView: UIView? { get }
}

class ImageDetailsTransition: NSObject, UIViewControllerAnimatedTransitioning {

    private let duration : TimeInterval
    private let isPresenting : Bool
    private var originFrame : CGRect
    
    private weak var transitionContext: UIViewControllerContextTransitioning?
    private var containerView: UIView? { return transitionContext?.containerView }
    private var fromView: UIView? { return transitionContext?.view(forKey: UITransitionContextViewKey.from) }
    private var fromVC: UIViewController? { return transitionContext?.viewController(forKey: .from) }
    private var toView: UIView? { return transitionContext?.view(forKey: UITransitionContextViewKey.to) }
    private var toVC: UIViewController? { return transitionContext?.viewController(forKey: .to) }
    
    init(duration : TimeInterval, isPresenting : Bool, originFrame : CGRect) {
        self.duration = duration
        self.isPresenting = isPresenting
        self.originFrame = originFrame
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        if isPresenting {
            present()
        } else {
            dissmis()
        }
    }
    
    private func present() {
        guard let transitionContext = transitionContext, let containerView = containerView, let toVC = toVC, let toView = toView else {
            fatalError("Present transition Error")
        }
        let fadeView = (toVC as? ImageDetailsTransitionedController)?.fadeView
        let finalFrame = transitionContext.finalFrame(for: toVC)
        let xScaleFactor = originFrame.width / finalFrame.width
        let yScaleFactor = originFrame.height / finalFrame.height
        let scaleTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)
        
        toView.transform = scaleTransform
        toView.center = CGPoint(x: originFrame.midX, y: originFrame.midY)
        toView.clipsToBounds = true
        containerView.addSubview(toView)
        fadeView?.backgroundColor = fadeView?.backgroundColor?.withAlphaComponent(0.0)
        
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseOut, animations: {
            toView.transform = .identity
            toView.center = CGPoint( x: finalFrame.midX, y: finalFrame.midY)
            fadeView?.backgroundColor = fadeView?.backgroundColor?.withAlphaComponent(1.0)
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
    
    private func dissmis(){
        guard let transitionContext = transitionContext, let containerView = containerView, let toView = toView, let fromView = fromView else {
            fatalError("Dismiss transition error")
        }
        
        let fadeView = (fromVC as? ImageDetailsTransitionedController)?.fadeView
        let compressedView = (fromVC as? ImageDetailsTransitionedController)?.compressedView
        
        containerView.insertSubview(toView, belowSubview: fromView)
        fromView.clipsToBounds = true
        
        if let fadeView = fadeView, let compressedView = compressedView {
            let finalFrame = originFrame
            originFrame = compressedView.frame
            (fadeView as? UIScrollView)?.zoomScale = 1.0
            
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseOut, animations: {
                compressedView.frame = finalFrame
                fadeView.backgroundColor = fadeView.backgroundColor?.withAlphaComponent(0.0)
            }, completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
            return
        }
        defaultDismiss()
    }
    
    private func defaultDismiss() {
        guard let transitionContext = transitionContext, let fromView = fromView else {
            fatalError("Dismiss transition error")
        }
        UIView.animate(withDuration: duration, delay: 0.0, options: [], animations: {
            fromView.alpha = 0.0
        }, completion: {_ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
