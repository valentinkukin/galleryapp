//
//  ActivitiesView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/8/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ActivitiesView: UIView {
    let headerView: UIView = UIView()
    let tableView: UITableView = UITableView()
    let cancelButton: UIButton = UIButton()
    let cleanButton: UIButton = UIButton()
    let addButton: UIButton = UIButton()
    let headerLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeHeaderView()
        initializeTableView()
        initializeCancleButton()
        initializeLabel()
        initializeCleanButton()
        initializeAddButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        let headerViewHeight = self.bounds.height/2.5
        let tableViewHeight = self.bounds.height - headerViewHeight
        headerView.frame = CGRect(origin: .zero, size: CGSize(width: self.bounds.width, height: headerViewHeight))
        let tableViewOrigin = CGPoint(x: 0, y: headerView.frame.maxY)
        tableView.frame = CGRect(origin: tableViewOrigin, size: CGSize(width: self.bounds.width, height: tableViewHeight))
        
        let parent = self.headerView.bounds
        let topPadding: CGFloat = 25
        let buttomPadding: CGFloat = 40
        let sidePadding: CGFloat = 10
        let manipulateButtonSize = CGSize(width: 30, height: 30)
        let cancelButtonSize = CGSize(width: 60, height: 30)
        let headerLabelHeight: CGFloat = 80
        
        let headerLabelOrigin = CGPoint(x: 0, y: parent.maxY - headerLabelHeight)
        let addButtonOrigin = CGPoint(x: sidePadding,y: parent.maxY - buttomPadding)
        let cleanButtonOrigin = CGPoint(x: parent.maxX - manipulateButtonSize.width - sidePadding, y: parent.maxY - buttomPadding)
        let cancleButtonOrigin = CGPoint(x: parent.maxX - cancelButtonSize.width - sidePadding, y: topPadding)
        
        headerLabel.frame = CGRect(origin: headerLabelOrigin, size: CGSize(width: parent.width, height: headerLabelHeight))
        cancelButton.frame = CGRect(origin: cancleButtonOrigin, size: cancelButtonSize)
        cleanButton.frame = CGRect(origin: cleanButtonOrigin, size: manipulateButtonSize)
        addButton.frame = CGRect(origin: addButtonOrigin, size: manipulateButtonSize)
    }
    
    private func initializeHeaderView() {
        headerView.backgroundColor = ThemeColor.current
        self.addSubview(headerView)
    }
    
    private func initializeTableView() {
        tableView.alwaysBounceVertical = false
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.tableFooterView = UIView()
        tableView.register(ActivitiesTableViewCell.self, forCellReuseIdentifier: ActivitiesTableViewCell.reuseID)
        self.addSubview(tableView)
    }
    
    private func initializeCancleButton() {
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.tintColor = .white
        headerView.addSubview(cancelButton)
    }
    
    private func initializeCleanButton() {
        cleanButton.setImage(UIImage(named: "clean.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cleanButton.tintColor = ThemeColor.current
        cleanButton.imageView?.contentMode = .scaleAspectFit
        cleanButton.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2,bottom: 2, right: 2)
        headerView.addSubview(cleanButton)
    }
    
    private func initializeAddButton() {
        addButton.setImage(UIImage(named: "add.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        addButton.tintColor = ThemeColor.current
        addButton.imageView?.contentMode = .scaleAspectFit
        addButton.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2,bottom: 2, right: 2)
        headerView.addSubview(addButton)
    }
    
    private func initializeLabel() {
        let text = "Your activities"
        let attributedString = NSMutableAttributedString(string: text.uppercased())
        let range = NSRange(location: 0, length: attributedString.string.count)
        attributedString.addAttributes([.kern: 5], range: range)
        attributedString.addAttributes([.font: UIFont.boldSystemFont(ofSize: 14)], range: range)
        
        headerLabel.attributedText = attributedString
        headerLabel.textColor = .black
        headerLabel.textAlignment = .center
        headerLabel.backgroundColor = .white
        
        headerView.addSubview(headerLabel)
    }
}
