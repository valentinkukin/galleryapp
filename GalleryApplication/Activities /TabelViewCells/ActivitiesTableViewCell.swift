//
//  ActivitiesTableViewCell.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/30/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ActivitiesTableViewCell: UITableViewCell {

    let senderLabel = UILabel()
    let messageLabel = UILabel()
    let dateLabel = UILabel()
    let arrowButton = UIButton()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let parent = contentView.bounds
        let topPadding: CGFloat = 5
        let arrowSide = parent.height - topPadding * 2
        arrowButton.frame = CGRect(x: parent.maxX - arrowSide, y: topPadding, width: arrowSide, height: arrowSide)
        let dateWidth: CGFloat = 60
        let dateHeight: CGFloat = 20
        dateLabel.frame = CGRect(x: arrowButton.frame.minX - dateWidth, y: topPadding, width: dateWidth, height: dateHeight)
        senderLabel.frame = CGRect(x: 10, y: topPadding, width: dateLabel.frame.minX, height: dateHeight)
        messageLabel.frame = CGRect(x: senderLabel.frame.minX, y: senderLabel.frame.maxY, width: arrowButton.frame.minX, height: 30)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ActivitiesTableViewCell {
    private func initializeViews() {
        initializeLabels()
        initializeButton()
    }
    
    private func initializeLabels() {
        senderLabel.textAlignment = .left
        senderLabel.font = UIFont.boldSystemFont(ofSize: 12)
        self.contentView.addSubview(senderLabel)
        
        dateLabel.textAlignment = .right
        dateLabel.font = UIFont.italicSystemFont(ofSize: 12)
        self.contentView.addSubview(dateLabel)
        
        messageLabel.textAlignment = .left
        messageLabel.font = UIFont.systemFont(ofSize: 14)
        
        self.contentView.addSubview(messageLabel)
    }
    
    private func initializeButton() {
        arrowButton.setImage(UIImage(named: "rightArrow.png")!, for: .normal)
        arrowButton.imageView?.contentMode = .scaleAspectFit
        arrowButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5,bottom: 5, right: 5)
        self.contentView.addSubview(arrowButton)
    }
}
