//
//  ActivitiesViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/30/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ActivitiesViewController: UIViewController {
    
    private let feedModel = ActivitiesFeedModel()
    
    private weak var cancelButton: UIButton?
    private weak var cleanButton: UIButton?
    private weak var addButton: UIButton?
    private weak var tableView: UITableView?
    
    override func loadView() {
        self.view = ActivitiesView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let activitiesView = self.view as? ActivitiesView
        cancelButton = activitiesView?.cancelButton
        cleanButton = activitiesView?.cleanButton
        addButton = activitiesView?.addButton
        tableView = activitiesView?.tableView
    
        cancelButton?.addTarget(self, action: #selector(cancelButtonClick), for: .touchUpInside)
        cleanButton?.addTarget(self, action: #selector(cleanButtonClick), for: .touchUpInside)
        addButton?.addTarget(self, action: #selector(addButtonClick), for: .touchUpInside)
        
        tableView?.dataSource = self
        tableView?.delegate = self
        feedModel.delegate = self
        feedModel.loadActivities()
    }
    
    @objc private func cancelButtonClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func cleanButtonClick(_ sender: UIButton) {
        feedModel.clear()
        tableView?.reloadData()
        setErrorView(errorText: "No Activities")
    }
    
    @objc private func addButtonClick(_ sender: UIButton) {
        feedModel.addTestNotification()
    }
}

extension ActivitiesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedModel.activitiesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivitiesTableViewCell.reuseID, for: indexPath) as! ActivitiesTableViewCell
        let activity = feedModel.activities[indexPath.row]
        
        cell.senderLabel.text = activity.sender ?? "n/n"
        cell.messageLabel.text = activity.text ?? "..."
        cell.dateLabel.text = dateToString(activity.date) ?? ""
        
        let lightCellColor = ThemeColor.current.withAlphaComponent(0.6)
        let darkCellColor = ThemeColor.current.withAlphaComponent(0.7)

        if feedModel.activitiesCount % 2 == 0 {
            cell.backgroundColor = (indexPath.row % 2 == 0) ? darkCellColor : lightCellColor
        } else {
            cell.backgroundColor = (indexPath.row % 2 == 0) ? lightCellColor : darkCellColor
        }
        
        return cell
    }
}

extension ActivitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension ActivitiesViewController: ActivitiesFeedModelDelegate {
    func didSaveActivity() {
        let indexPaths = [IndexPath(item: 0, section: 0)]
        tableView?.backgroundView = nil
        tableView?.insertRows(at: indexPaths, with: .left)
    }

    func didLoadActivities() {
        tableView?.reloadData()
    }
    
    func didFailInSavingActivity() {
        NSLog("Did fail in saving activity")
    }
    
    func didFailInLoadActivities() {
        setErrorView(errorText: "No Activities")
    }
    
    func setErrorView(errorText: String) {
        guard tableView?.numberOfRows(inSection: 0) == 0 else {
            return
        }
        let errorLabel = UILabel()
        errorLabel.text = errorText
        errorLabel.textAlignment = .center
        errorLabel.backgroundColor = Color.errorBackground
        tableView?.backgroundView = errorLabel
    }
}

fileprivate let dateFormatter = DateFormatter()
fileprivate func dateToString(_ date: Date?) -> String? {
    guard let date = date else {
        return nil
    }

    if date.isSameDate(Date()) {
        dateFormatter.dateFormat = "hh:mm"
    } else {
        dateFormatter.dateFormat = "dd.MM"
    }

    let dateString = dateFormatter.string(from: date)
    return dateString
}

extension Date {
    func isSameDate(_ comparisonDate: Date) -> Bool {
        let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
        return order == .orderedSame
    }
}
