//
//  SplashScreenViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/27/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

protocol SplashScreenDelegate : class {
    func didLoad()
}

class SplashScreenViewController: UIViewController {
    
    weak var delegate: SplashScreenDelegate?
    
    override func loadView() {
        self.view = SplashScreenView()
    }

    override func viewDidAppear(_ animated: Bool) {
        (view as? SplashScreenView)?.startAnimation()
        load()
    }
    
    private func load() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // Just for imitation some work
            self.delegate?.didLoad()
        }
    }
}
