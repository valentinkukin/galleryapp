//
//  SplashScreenView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/9/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SplashScreenView: UIView {
    private let squerView = UIView()
    private var labels: [UILabel] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        initializeSquerView()
        initializeLabels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        squerView.frame.size = CGSize(width: 80, height: 80)
        squerView.center = CGPoint(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.midY)
        layoutLabels()
    }
    
    private func initializeSquerView() {
        squerView.backgroundColor = ThemeColor.current
        addSubview(squerView)
    }
    
    private func initializeLabels() {
        for letter in "Loading" {
            let font: UIFont = .systemFont(ofSize: 30)
            let label = UILabel()
            label.text = String(letter)
            label.textColor = .white
            label.backgroundColor = ThemeColor.current
            label.textAlignment = .center
            label.font = font
            label.alpha = 0
            
            labels.append(label)
            addSubview(label)
        }
    }
    
    private func layoutLabels() {
        let count: CGFloat = CGFloat(labels.count)
        let cardWidth: CGFloat = 25
        let cardHeight: CGFloat = 40
        let spacing: CGFloat = 2
        let sumWidth = cardWidth * count + spacing * (count - 1)
        let originX = UIScreen.main.bounds.width * 0.5 - sumWidth * 0.5
        
        for (i, label) in labels.enumerated() {
            label.frame.origin.x = originX + CGFloat(i) * (cardWidth + spacing)
            label.frame.size = CGSize(width: cardWidth, height: cardHeight)
            label.center.y = UIScreen.main.bounds.midY + 80
        }
    }
}

extension SplashScreenView {
    
    func startAnimation() {
        startSquerViewAnimation()
        startLabelsAnimation()
    }
    
    private func startSquerViewAnimation() {
        UIView.animate(withDuration: 1,delay: 0, options: [.repeat, .autoreverse], animations: {
            self.squerView.transform = CGAffineTransform(scaleX: 2, y: 2)
            self.squerView.backgroundColor = ThemeColor.current.withAlphaComponent(0.0)
        }, completion: nil)
    }
    
    private func startLabelsAnimation() {
        var delay: TimeInterval = 0
        for (i, label) in labels.enumerated() {
            let duration: TimeInterval = 0.5 + TimeInterval(labels.count - i) * 0.04
            delay = delay + duration * 0.5
            UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2,
                           options: .curveEaseOut, animations: {
                            label.alpha = 1
            })
        }
    }
}
