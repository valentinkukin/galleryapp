//
//  SettingsView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SettingsView: UIView {
    
    let tableView = UITableView()
    let headerView = UIView()
    let headerLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        initializeHeaderView()
        initializeHeadelLabel()
        initializeTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        let headerViewHeight = bounds.height/8
        headerView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: headerViewHeight)
        headerLabel.frame = headerView.bounds
        tableView.frame = CGRect(x: 0, y: headerView.frame.maxY, width: bounds.width, height: bounds.height - headerViewHeight)
    }

    private func initializeHeaderView() {
        headerView.backgroundColor = ThemeColor.current
        addSubview(headerView)
    }
    
    private func initializeHeadelLabel() {
        headerLabel.font = .systemFont(ofSize: 25)
        headerLabel.text = "Settings"
        headerLabel.textColor = .white
        headerLabel.textAlignment = .center
        headerView.addSubview(headerLabel)
    }
    
    private func initializeTableView() {
        tableView.alwaysBounceVertical = false
        tableView.tableFooterView = UIView()
        tableView.register(SettingCell.self, forCellReuseIdentifier: SettingCell.reuseID)
        addSubview(tableView)
    }

}
