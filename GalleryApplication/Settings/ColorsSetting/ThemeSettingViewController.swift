//
//  ColorsSettingViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

protocol ThemeSettingDelegate: class {
    func didChangeThemeColor()
}

class ThemeSettingViewController: UIViewController {

    private weak var okButton: UIButton?
    weak var delegate: ThemeSettingDelegate?
    
    override func loadView() {
        view = ThemeSettingView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let themeSettingView = view as? ThemeSettingView
        okButton = themeSettingView?.okButton
        okButton?.addTarget(self, action: #selector(okButtonClick), for: .touchUpInside)
        
        guard let colorButtons = themeSettingView?.colorButtons else {
            return
        }
        for button in colorButtons {
            button.addTarget(self, action: #selector(colorButtonClick), for: .touchUpInside)
        }
    }
    
    @objc private func okButtonClick(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @objc private func colorButtonClick(_ sender: UIButton) {
        guard let color = sender.backgroundColor else {
            return
        }
        ThemeColor.setCurrent(color: color)
        delegate?.didChangeThemeColor()

        let themeSettingView = view as? ThemeSettingView
        guard let colorButtons = themeSettingView?.colorButtons else {
            return
        }
        for button in colorButtons {
            button.layer.borderWidth = 0
        }
        sender.layer.borderWidth = 2
    }
}
