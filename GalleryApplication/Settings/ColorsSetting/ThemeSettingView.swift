//
//  ColorsSettingView.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ThemeSettingView: UIView {
    let titleLabel = UILabel()
    let contentView = UIView()
    let okButton = UIButton()
    private(set) var colorButtons: [UIButton] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        initializeContentView()
        initializeTitleLabel()
        initializeOkButton()
        initializeColorButtons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        contentView.frame.size = CGSize(width: 300, height: 200)
        contentView.center = center
        
        let parent = contentView.bounds
        let titleLabelHeight: CGFloat = 60
        let buttonSize = CGSize(width: 60, height: 40)
        let padding: CGFloat = 5
        let okButtonOriginX = parent.maxX - buttonSize.width - padding
        let okButtonOriginY = parent.maxY - buttonSize.height - padding
        okButton.frame = CGRect(x: okButtonOriginX, y: okButtonOriginY, width: buttonSize.width, height: buttonSize.height)
        titleLabel.frame = CGRect(x: 0, y: 0, width: parent.width, height: titleLabelHeight)
        layoutButtons()
    }
    
    private func initializeContentView() {
        contentView.backgroundColor = .white
        addSubview(contentView)
    }
    
    private func initializeTitleLabel() {
        titleLabel.text = "Theme color: "
        titleLabel.textAlignment = .center
        titleLabel.font = .systemFont(ofSize: 20)
        contentView.addSubview(titleLabel)
    }
    
    private func initializeOkButton() {
        okButton.setTitle("Ok", for: .normal)
        okButton.setTitleColor(.white, for: .normal)
        okButton.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        okButton.layer.cornerRadius = 5
        contentView.addSubview(okButton)
    }
    
    private func layoutButtons() {
        let count: CGFloat = CGFloat(colorButtons.count)
        let buttonSize = CGSize(width: 50, height: 50)
        let spacing: CGFloat = 10
        let sumWidth = buttonSize.width * count + spacing * (count - 1)
        let originX = contentView.bounds.width * 0.5 - sumWidth * 0.5
        
        for (i, button) in colorButtons.enumerated() {
            button.frame.origin.x = originX + CGFloat(i) * (buttonSize.width + spacing)
            button.frame.size = buttonSize
            button.center.y = contentView.bounds.midY
        }
    }
    
    private func initializeColorButtons() {
        createColorButton(withColor: ThemeColor.green)
        createColorButton(withColor: ThemeColor.red)
        createColorButton(withColor: ThemeColor.yellow)
        createColorButton(withColor: ThemeColor.blue)
        
        for button in colorButtons {
            button.layer.cornerRadius = 5
            button.layer.borderColor = UIColor.green.cgColor
            
            if button.backgroundColor == ThemeColor.current {
                button.layer.borderWidth = 2
            } else {
                button.layer.borderWidth = 0
            }
        }
    }
    
    private func createColorButton(withColor color: UIColor) {
        let button = UIButton()
        button.backgroundColor = color
        contentView.addSubview(button)
        colorButtons.append(button)
    }
}
