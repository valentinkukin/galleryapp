//
//  SettingsViewController.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    private var sections: [Section] = []
    private weak var tableView: UITableView?

    override func loadView() {
        view = SettingsView()
        initializeSections()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = (view as? SettingsView)?.tableView
        tableView?.delegate = self
        tableView?.dataSource = self
    }
}

extension SettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingCell.reuseID, for: indexPath) as! SettingCell
        cell.settingLabel.text = sections[indexPath.section].items[indexPath.row].title
        return cell
    }
}

extension SettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sections[indexPath.section].items[indexPath.row].action()
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
}

extension SettingsViewController: ThemeSettingDelegate {
    func didChangeThemeColor() {
        (view as? SettingsView)?.headerView.backgroundColor = ThemeColor.current
        tabBarController?.tabBar.tintColor = ThemeColor.current
    }
}

extension SettingsViewController {
    
    private func initializeSections() {
        let colorSeciton = Section(title: "Colors", items: [Item(title: "Theme", action: presentThemeSetting)])
        sections = [colorSeciton]
    }
    
    private func presentThemeSetting() {
        let themeVC = ThemeSettingViewController()
        themeVC.modalPresentationStyle = .overFullScreen
        themeVC.modalTransitionStyle = .crossDissolve
        themeVC.delegate = self
        present(themeVC, animated: true, completion: nil)
    }
}

fileprivate struct Section {
    let title: String
    let items: [Item]
}

fileprivate struct Item {
    let title: String
    let action: ()->Void
}
