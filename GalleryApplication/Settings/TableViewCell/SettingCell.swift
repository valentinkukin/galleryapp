//
//  SettingCell.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    let settingLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeLabel()
    }
    
    override func layoutSubviews() {
        let leftPadding: CGFloat = 15
        settingLabel.frame = CGRect(x: leftPadding, y: 0, width: bounds.width - leftPadding, height: bounds.height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeLabel() {
        settingLabel.textAlignment = .left
        addSubview(settingLabel)
    }
    

}
