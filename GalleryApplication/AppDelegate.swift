//
//  AppDelegate.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/16/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//
//API Key for unsplas.com - bc277a6d26092281976c5f0210a8997a58de8e82a848c2b506a61fdf001302ca
//Access Token instagram - 8468097535.1677ed0.7656293aeeba470faf51ee264c219680

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window

        let splashViewController = SplashScreenViewController()
        splashViewController.delegate = self
        window.rootViewController = splashViewController
        window.makeKeyAndVisible()
        
        UserNotificationManager.shared.enablePushNotifications()
        
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        UserNotificationManager.shared.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UserNotificationManager.shared.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
    }
}

extension AppDelegate: SplashScreenDelegate {
    func didLoad() {
        let tabBarController = UITabBarController()
        initialize(tabBarController)
        window?.rootViewController = tabBarController
    }
    
    private func initialize(_ tabBarController: UITabBarController) {
        let galleryController = UINavigationController(rootViewController: GalleryViewController())
        let savedPotosController = UINavigationController(rootViewController: SavedPhotosViewController())
        let instagramVideoController = InstagramVideoViewController()
        let settingsViewController = SettingsViewController()
        let cameraViewController = CameraViewController()
        
        initialize(galleryController, title: "Gallery", image: UIImage(named: "gallery.png"))
        initialize(savedPotosController, title: "Saved Photos", image: UIImage(named:"saved.png"))
        initialize(instagramVideoController, title: "Instagram", image: UIImage(named:"instagram"))
        initialize(settingsViewController, title: "Settings", image: UIImage(named: "settings.png"))
        initialize(cameraViewController, title: "Camera", image: UIImage(named: "camera.png"))
        
        UITabBar.appearance().tintColor = ThemeColor.current
        tabBarController.viewControllers = [galleryController, savedPotosController,
                                            instagramVideoController, cameraViewController,
                                            settingsViewController]
    }
    
    private func initialize(_ vc: UIViewController, title: String, image: UIImage?) {
        vc.tabBarItem.title = title
        vc.tabBarItem.image = image
    }
}

