//
//  File.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/3/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import ActivitiesDataCore

class UserNotificationManager: NSObject {
    static var shared = UserNotificationManager()
    
    func enablePushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            guard granted else {
                return
            }
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                guard settings.authorizationStatus == .authorized  else {
                    return
                }
                DispatchQueue.main.sync {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenStr = deviceToken.map{ String(format: "%02.2hhx", Int($0)) }.joined()
        print("Device token:", deviceTokenStr)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handle(userInfo)
    }
    
    private func handle(_ userInfo: [AnyHashable: Any]) {
        guard let userInfo = userInfo as? [String: Any] else {
            return
        }
        guard let sender = userInfo["sender"] as? String, let text = userInfo["text"] as? String else {
            return
        }
        ActivitiesManager.shared.save(ActivityNotification(text: text, sender: sender, date: Date()))
    }
}
