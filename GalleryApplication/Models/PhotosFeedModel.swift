//
//  PhotosManagerModel.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/26/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit
import Photos
import PhotosCore

protocol PhotosFeedModelDelegate: class {
    func didSavePhotos(at indexPath: [IndexPath])
    func didRemovePhotos(at indexPath: [IndexPath])
    func didChangePhotos(at indexPath: [IndexPath])
}

class PhotosFeedModel: NSObject {
    private let photosManager = PhotosManager.shared
    private var assets: PHFetchResult<PHAsset> = PHFetchResult<PHAsset>()
    let album = PhotosSettings.album
    var assetsCount: Int { return assets.count }

    weak var delegate: PhotosFeedModelDelegate?
    
    override init() {
        super.init()
        PHPhotoLibrary.shared().register(self)
    }
    
    func loadAssets(completion: (()->Void)? = nil) {
        photosManager.loadAssets(from: album) { assets in
            self.assets = assets
            completion?()
        }
    }
}

extension PhotosFeedModel: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {

        guard let change = changeInstance.changeDetails(for: assets) else {
            return
        }
        guard change.hasIncrementalChanges else {
            return
        }
        
        assets = change.fetchResultAfterChanges
        DispatchQueue.main.async {
            if let removed = change.removedIndexes, removed.count > 0 {
                let indexPaths = removed.map { IndexPath(item: $0, section:0) }
                self.delegate?.didRemovePhotos(at: indexPaths)
            }
            if let inserted = change.insertedIndexes, inserted.count > 0 {
                let indexPaths = inserted.map { IndexPath(item: $0, section:0) }
                self.delegate?.didSavePhotos(at: indexPaths)
            }
            if let changed = change.changedIndexes, changed.count > 0 {
                let indexPaths = changed.map { IndexPath(item: $0, section:0) }
                self.delegate?.didChangePhotos(at: indexPaths)
            }
        }
    }
}

extension PhotosFeedModel: ImageFeedModel {
    
    func setImage(at index: Int, for view: UIImageView, compressed: Bool, placeHolder: UIImage?) {
        let asset = assets[index]
        let imgSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
        let size = (compressed) ? CGSize.compressed(size: imgSize) : imgSize
        
        view.setImage(with: asset, size: size, placeHolder: placeHolder)
    }
}

extension CGSize {
    static func compressed(size: CGSize) -> CGSize {
        let iw = CGFloat(350)
        guard iw < size.width else {
            return size
        }
        
        let ar = size.width/size.height
        let ih = (iw/ar).rounded()
        return CGSize(width: iw, height: ih)
    }
}
