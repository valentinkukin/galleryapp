//
//  FeedModel.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/17/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import HttpCore

protocol FeedModelDelegate: class {
    func didLoadImages(count: Int)
    func didFailInLoadingImages(errorText: String?)
}

class GalleryFeedModel {
    enum State {
        case inDefault
        case inSearch
    }
    
    private(set) var state: State = .inDefault
    private(set) var images: [ImageInfo] = []
    private var imageRequest: ImageRequest!
    private var currentPage: Int = 0
    private let defaultRequestParameters: [String: Any] = ["page": 1, "per_page": 8]
    var imagesCount: Int { return images.count }
    
    weak var delegate: FeedModelDelegate?
    
    init() {
        setDefaultImageRequest()
    }

    func loadImagesFromNextPage() {
        currentPage += 1
        imageRequest.setPage(currentPage)
        imageRequest.send()
    }
    
    func clear() {
        images = []
    }
    
    func loadImages(query: String? = nil) {
        if let query = query {
            setSearchImageRequest(query: query)
        } else {
            setDefaultImageRequest()
        }
        currentPage = 0
        loadImagesFromNextPage()
    }
    
    private func setDefaultImageRequest() {
        state = .inDefault
        setImageRequest(parameters: ["page": 1, "per_page": 8], parser: Parsers.defaultImagesParser, route: .photos)
    }
    
    private func setSearchImageRequest(query: String) {
        guard state == .inDefault else {
            imageRequest.params["query"] = query
            return
        }
        state = .inSearch
        setImageRequest(parameters: ["page": 1, "per_page": 8, "query": query], parser: Parsers.searchImagesParser, route: .search)
    }
    
    private func setImageRequest(parameters: [String: Any], parser: @escaping ImageParser, route: Route) {
        imageRequest = RequestBuilder {
            return ImageRequest(parameters: parameters, parser: parser)
            }
            .route(route)
            .make()
        imageRequest.delegate = self
    }
}

extension GalleryFeedModel: ImageRequestDelegate {
    func didFail(with error: ImageRequestErrors) {
        delegate?.didFailInLoadingImages(errorText: error.errorDescription)
    }
    
    func didLoad(images: [ImageInfo]) {
        self.images += images
        
        guard !images.isEmpty else {
            delegate?.didFailInLoadingImages(errorText: "Can not find any images")
            return
        }
        
        delegate?.didLoadImages(count: images.count)
    }
}

extension GalleryFeedModel: ImageFeedModel {
    func setImage(at index: Int, for view: UIImageView, compressed: Bool, placeHolder: UIImage?) {
        let url: String = (compressed) ? images[index].thumbURL : images[index].regularURL
        view.af_setImage(withURL: URL(string: url)!, placeholderImage: placeHolder)
    }
}
