//
//  ActivitiesFeedModel.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/31/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import ActivitiesDataCore

protocol ActivitiesFeedModelDelegate: class {
    func didSaveActivity()
    func didFailInSavingActivity()
    func didLoadActivities()
    func didFailInLoadActivities()
}

class ActivitiesFeedModel {
    
    private let activitiesManager = ActivitiesManager.shared
    weak var delegate: ActivitiesFeedModelDelegate?
    
    private(set) var activities = [Activity]()
    var activitiesCount: Int { return activities.count }
    
    init() {
        activitiesManager.delegate = self
    }
    
    func loadActivities() {
        activitiesManager.fetchActivities()
    }
    
    func clear() {
        activities = []
        activitiesManager.clearActivities()
    }
    
    func addTestNotification() {
        activitiesManager.save(ActivityNotification(text: "Test \(activities.count + 1)", sender: "Me", date: Date()))
    }
}



extension ActivitiesFeedModel: ActivitiesManagerDelegate {
    
    func didSave(activity: Activity) {
        activities.insert(activity, at: 0)
        delegate?.didSaveActivity()
    }
    
    func didFailInSavingActivities(with error: ActivitiesRequestErrors) {
        delegate?.didFailInSavingActivity()
    }
    
    func didLoad(activities: [Activity]) {
        self.activities = activities.reversed()
        delegate?.didLoadActivities()
    }
    
    func didFailInLoadActivities(with error: ActivitiesRequestErrors) {
        delegate?.didFailInLoadActivities()
    }
}

