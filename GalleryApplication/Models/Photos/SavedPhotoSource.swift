//
//  SavedPhotoSource.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 7/26/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit

class SavedPhotosSource {
    var images: [UIImage] = []
    init() {
        PhotosManagerModel.delegate = self
    }
    func loadImages(from album: String) {
        PhotosManagerModel.getImages(from: album)
    }
}

extension SavedPhotosSource: PhotosManagerDelegate {
    func didLoad(images: [UIImage], from album: String) {
        self.images = images
        print(images.count)
    }
    
}
