//
//  ImageFeedModel.swift
//  GalleryApplication
//
//  Created by Kukin Valentin on 8/17/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import UIKit

protocol ImageFeedModel {
    func setImage(at index: Int, for view: UIImageView, compressed: Bool, placeHolder: UIImage?)
}
