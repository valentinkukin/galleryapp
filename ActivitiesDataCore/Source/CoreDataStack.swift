//
//  CoreDataStack.swift
//  ActivitiesDataCore
//
//  Created by Kukin Valentin on 7/31/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    init() { }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let bundle = Bundle(for: CoreDataStack.self)
        let url = bundle.url(forResource: "ActivitiesDataModel", withExtension: "momd")!
        let model = NSManagedObjectModel(contentsOf: url)!
        let container = NSPersistentContainer(name: "ActivitiesDataModel", managedObjectModel: model)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
