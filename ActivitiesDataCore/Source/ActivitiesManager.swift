//
//  ActivitiesRequest.swift
//  ActivitiesDataCore
//
//  Created by Kukin Valentin on 7/31/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation
import CoreData

public protocol ActivitiesManagerDelegate: class {
    func didSave(activity: Activity)
    func didFailInSavingActivities(with error: ActivitiesRequestErrors)
    func didLoad(activities: [Activity])
    func didFailInLoadActivities(with error: ActivitiesRequestErrors)
}

public class ActivitiesManager {
    public static let shared = ActivitiesManager()
    
    private let coreDataQueue = DispatchQueue(label: "coreDataQueue")
    private let entityName = "Activity"
    private let coreDataStack = CoreDataStack()
    
    public weak var delegate: ActivitiesManagerDelegate?
    
    public init() {}
    
    public func save(_ activityNotification: ActivityNotification) {
        let managedContext = coreDataStack.persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedContext) else {
            return
        }
        
        let activity = NSManagedObject(entity: entity, insertInto: managedContext) as? Activity
        activity?.date = activityNotification.date
        activity?.sender = activityNotification.sender
        activity?.text = activityNotification.text
        
        do {
            try managedContext.save()
            if let activity = activity {
                delegate?.didSave(activity: activity)
            }
        } catch {
            delegate?.didFailInSavingActivities(with: ActivitiesRequestErrors.invalidRequest)
        }
    }
    
    public func fetchActivities() {
        let managedContext = coreDataStack.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)

        let asyncFetchRequest = NSAsynchronousFetchRequest(fetchRequest: fetchRequest) { result in
            guard let result = result.finalResult, let activities = result as? [Activity], !activities.isEmpty else {
                self.delegate?.didFailInLoadActivities(with: ActivitiesRequestErrors.activitiesNotFound)
                return
            }
            self.delegate?.didLoad(activities: activities)
        }
        do {
            _ = try managedContext.execute(asyncFetchRequest)
        } catch {
            self.delegate?.didFailInLoadActivities(with: ActivitiesRequestErrors.invalidRequest)
        }
    }
    
    public func clearActivities() {
        let managedContext = coreDataStack.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            _ = try managedContext.execute(deleteRequest)
        } catch {
            self.delegate?.didFailInLoadActivities(with: ActivitiesRequestErrors.invalidRequest)
        }
    }
}

public struct ActivityNotification {
    public let text: String
    public let sender: String
    public let date: Date
    
    public init(text: String, sender: String, date: Date) {
        self.text = text
        self.sender = sender
        self.date = date
    }
}

public enum ActivitiesRequestErrors: LocalizedError {
    case activitiesNotFound
    case invalidRequest
    
    public var errorDescription: String? {
        switch self {
        case .invalidRequest:
            return "Invalid request."
        case .activitiesNotFound:
            return "Activities have not founded"
        }
    }
}


//{
//    "aps": {
//        "alert":{
//            "title":"UPark",
//            "body":"Muse Drones World Tour in Kiev, do not miss!"
//        },
//        "badge":1,
//        "sound":"default",
//        "content-available":1
//    },
//    "sender":"UPark",
//    "text":"Muse Drones World Tour in Kiev, do not miss!"
//}
